@extends('frontend.layouts.app')

@inject('orderPresenter', 'App\Model\Presenters\Order\OrderPresenter')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $order->sn }} -

                        {{ $orderPresenter->getDateFormatText($order->created_at) }}
                        {{ $order->present()->getDateFormatText() }}
                    </div>
                    <div class="panel-body">
                        {{ $orderPresenter->getStatusText($order->status) }}
                        {{ $order->present()->getStatusText() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
