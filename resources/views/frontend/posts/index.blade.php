@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @foreach($posts as $post)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ action('Frontend\PostsController@show', $post->id) }}">{{ $post->title }}</a>
                    </div>
                    <div class="panel-body">
                        {{ $post->created_at->toDateTimeString() }}

                        @if (Auth::id() === $post->user_id)
                            <div class="pull-right">
                                <form action="{{ action('Frontend\PostsController@destroy', $post->id) }}" method="post">
                                    {!! csrf_field() !!}
                                    {!! method_field('delete') !!}

                                    <a href="{{ action('Frontend\PostsController@edit', $post->id) }}">
                                        <button type="button" class="btn btn-primary">Edit</button>
                                    </a>
                                    <button type="submit" class="btn btn-primary">Delete</button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
