@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $post->title }} - {{ $post->created_at->toFormattedDateString() }}
                </div>
                <div class="panel-body">
                    {{ $post->content }}
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Comments
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($comments as $comment)
                            <li class="list-group-item">
                                {{ $comment->content }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <form action="{{ action('CommentsController@store', $post->id) }}" method="post">
                {!! csrf_field() !!}

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Comment
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
