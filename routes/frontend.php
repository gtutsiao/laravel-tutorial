<?php

use App\Model\Entities\Post\Post;
use App\Model\Entities\Post\Tag;

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', 'HomeController@index');

    Auth::routes();

    Route::get('home', 'HomeController@index')
         ->name('home');

    // Route::get('posts', 'PostsController@index');
    // Route::get('posts/create', 'PostsController@create');
    // Route::post('posts', 'PostsController@store');
    // Route::get('posts/{id}', 'PostsController@show');
    // Route::get('posts/{id}/edit', 'PostsController@edit');
    // Route::put('posts/{id}', 'PostsController@update');
    // Route::delete('posts/{id}', 'PostsController@destroy');

    Route::resource('posts', 'PostsController');
    Route::post('posts/{postId}/create-comment', 'CommentsController@store');

    Route::get('lang', function () {
        //    config()->set('app.locale', 'en');
        config()->set('app.locale', 'zh-TW');

        echo trans('auth.failed');
    });

    Route::get('errors/{code}', function ($code) {
        abort($code);
    });

    Route::get('test', function () {
        dd(Post::find(1)->tags);
        dd(Tag::find(1)->posts);
    });

    Route::get('session', function () {
        session()->get('key', 'default');
        session('key', 'default');

        session(['key' => 'value']);
        session()->put('key', 'value');

        session()->forget('key');

        session()->flush();

        dd(session()->all());
    });

    Route::get('cache', function () {
        cache()->put('key', 'default', 10);

        cache()->flush();

        // dd(cache()->get('key'));

        $value = Cache::get('key', function () {
            return Post::first();
        });

        $value = Cache::remember('key', 10, function () {
            return Post::first();
        });

        dd($value);
    });

    Route::post('orders', 'OrderController@store');
    Route::get('orders/{sn}', 'OrderController@show');
});