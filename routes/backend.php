<?php

Route::group(['namespace' => 'Backend'], function () {
    Route::get('/', 'HomeController@index');

    Auth::routes();
});