<?php

namespace App\Services\Order;

use App\Model\Entities\Order\Order;
use App\Model\Repositories\Order\OrderProductRepository;
use App\Model\Repositories\Order\OrderRepository;
use App\Model\Repositories\Order\OrderStatusRepository;
use App\Model\Repositories\Product\ProductRepository;
use Illuminate\Database\Eloquent\Model;

class OrderService
{

    private $orderProductRepository;
    private $orderRepository;
    private $orderStatusRepository;
    private $productRepository;

    /**
     * OrderService constructor.
     *
     * @param OrderRepository        $orderRepository
     * @param OrderProductRepository $orderProductRepository
     * @param OrderStatusRepository  $orderStatusRepository
     * @param ProductRepository      $productRepository
     */
    public function __construct(
        OrderRepository $orderRepository,
        OrderProductRepository $orderProductRepository,
        OrderStatusRepository $orderStatusRepository,
        ProductRepository $productRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderProductRepository = $orderProductRepository;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * 新增訂單。
     *
     * @param array $data
     *
     * @return Order|Model
     */
    public function createOrder($data)
    {
        $data = collect($data);

        // 新增訂單。
        $order = $this->orderRepository->create();

        // 新增訂單商品。
        $orderProducts = [];

        foreach ($data->get('product_ids', []) as $index => $productId) {
            $quantity = $data->get('product_quantities')[$index];

            $orderProducts[] = [
                'order_id' => $order->id,
                'product_id' => $productId,
                'quantity' => $quantity
            ];

            // 扣除商品庫存。
            $this->productRepository->deductStock($productId, $quantity);
        }

        $this->orderProductRepository->insert($orderProducts);

        // 新增訂單狀態。
        $this->orderStatusRepository->create([
            'order_id' => $order->id,
            'status' => 1
        ]);

        return $order;
    }
}