<?php

namespace App\Model\Repositories\Order;

use App\Model\Entities\Order\Order;

class OrderRepository
{

    private $model;

    /**
     * OrderRepository constructor.
     *
     * @param Order $model
     */
    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    /**
     * 新增訂單。
     *
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function create()
    {
        return $this->model->create([
            'user_id' => auth()->id(),
            'sn' => str_random(16),
            'status' => 1
        ]);
    }

    /**
     * 透過編號取得訂單。
     *
     * @param string $sn
     *
     * @return Order|null|object
     */
    public function findBySn($sn)
    {
        return $this->model->where('sn', $sn)
                           ->first();
    }
}