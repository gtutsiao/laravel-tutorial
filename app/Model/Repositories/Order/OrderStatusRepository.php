<?php

namespace App\Model\Repositories\Order;

use App\Model\Entities\Order\OrderStatus;

class OrderStatusRepository
{

    private $model;

    /**
     * OrderStatusRepository constructor.
     *
     * @param OrderStatus $model
     */
    public function __construct(OrderStatus $model)
    {
        $this->model = $model;
    }

    /**
     * 新增訂單狀態。
     *
     * @param array $data
     */
    public function create($data)
    {
        $this->model->create($data);
    }
}