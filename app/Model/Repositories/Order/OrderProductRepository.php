<?php

namespace App\Model\Repositories\Order;

use App\Model\Entities\Order\OrderProduct;

class OrderProductRepository
{

    private $model;

    /**
     * OrderProductRepository constructor.
     *
     * @param OrderProduct $model
     */
    public function __construct(OrderProduct $model)
    {
        $this->model = $model;
    }

    /**
     * 新增訂單商品。
     *
     * @param array $orderProducts
     */
    public function insert($orderProducts)
    {
        $this->model->insert($orderProducts);
    }
}