<?php

namespace App\Model\Repositories\Product;

use App\Model\Entities\Product\Product;

class ProductRepository
{

    private $model;

    /**
     * ProductRepository constructor.
     *
     * @param Product $model
     */
    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    /**
     * 扣除商品庫存。
     *
     * @param int $productId
     * @param int $quantity
     */
    public function deductStock($productId, $quantity)
    {
        $product = $this->model->find($productId);
        $product->quantity = $product->quantity - $quantity;
        $product->save();
    }

}