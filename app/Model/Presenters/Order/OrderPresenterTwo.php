<?php

namespace App\Model\Presenters\Order;

use App\Model\Presenters\AbstractPresenter;

class OrderPresenterTwo extends AbstractPresenter
{

    /**
     * 取得格式化後時間。
     *
     * @return mixed
     */
    public function getDateFormatText()
    {
        switch (app()->getLocale()) {
            case 'en':
                return $this->created_at->format('Y-m-d H:i:s');
                break;
            case 'es':
                return $this->created_at->format('Y-m-d H:i:s');
                break;
            default:
                return $this->created_at->format('Y-m-d H:i:s');
        }
    }

    /**
     * 取得狀態文字。
     *
     * @return string
     */
    public function getStatusText()
    {
        $statusList = [
            1 => '訂單已建立',
            2 => '訂單已付款',
            3 => '訂單完成'
        ];

        return $statusList[$this->status];
    }
}