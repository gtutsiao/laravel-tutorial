<?php

namespace App\Model\Presenters\Order;

use Carbon\Carbon;

class OrderPresenter
{

    /**
     * 取得格式化後時間。
     *
     * @param Carbon $date
     *
     * @return mixed
     */
    public function getDateFormatText($date)
    {
        switch (app()->getLocale()) {
            case 'en':
                return $date->format('Y-m-d H:i:s');
                break;
            case 'es':
                return $date->format('Y-m-d H:i:s');
                break;
            default:
                return $date->format('Y-m-d H:i:s');
        }
    }

    /**
     * 取得狀態文字。
     *
     * @param int $status
     *
     * @return string
     */
    public function getStatusText($status)
    {
        $statusList = [
            1 => '訂單已建立',
            2 => '訂單已付款',
            3 => '訂單完成'
        ];

        return $statusList[$status];
    }
}