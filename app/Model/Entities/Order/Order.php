<?php

namespace App\Model\Entities\Order;

use App\Model\Entities\User\User;
use App\Model\Presenters\Order\OrderPresenterTwo;
use App\Model\Presenters\PresentableTrait;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    use PresentableTrait;

    protected $presenter = OrderPresenterTwo::class;
    protected $fillable = ['user_id', 'sn', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'order_id');
    }

    public function orderStatus()
    {
        return $this->hasMany(OrderStatus::class, 'order_id');
    }
}
