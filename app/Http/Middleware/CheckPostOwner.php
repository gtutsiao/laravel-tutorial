<?php

namespace App\Http\Middleware;

use App\Post;
use Auth;
use Closure;

class CheckPostOwner
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $postId = $request->route()
                          ->parameter('post');

        $post = Post::find($postId);

        if (Auth::id() !== $post->user_id) {
            return redirect()
                ->action('PostsController@index')
                ->withErrors('沒有權限');
        }

        return $next($request);
    }
}