<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Model\Entities\Post\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function store(CommentRequest $request, $postId)
    {
        Comment::create([
            'post_id' => $postId,
            'content' => $request->get('content')
        ]);

        return redirect()->action('Frontend\PostsController@show', $postId);
    }
}