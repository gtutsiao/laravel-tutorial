<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Model\Entities\Post\Comment;
use App\Model\Entities\Post\Post;
use Auth;
use Validator;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('checkPostOwner')
             ->only(['edit', 'update', 'delete']);
    }

    public function index()
    {
        $posts = Post::orderBy('id', 'DESC')
                     ->get();

        return view('frontend.posts.index', compact('posts'));
    }

    public function show($id)
    {
        $post = Post::find($id);

        // $comments = Comment::where('post_id', $id)
        //                    ->get();

        $comments = $post->comments;

        dd($post->tags);

        return view('frontend.posts.show', compact('post', 'comments'));
    }

    public function create()
    {
        return view('frontend.posts.create');
    }

    public function store(PostRequest $request)
    {
        // $this->validate(request(), [
        //     'title' => 'required',
        //     'content' => 'required'
        // ]);

        // $validator = Validator::make(request()->all(), [
        //     'title' => 'required',
        //     'body' => 'required',
        // ]);

        // if ($validator->fails()) {
        //     return redirect()
        //         ->back()
        //         ->withErrors($validator);
        // }

        $post = new Post();
        $post->user_id = Auth::id();
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->save();

        //        Post::create([
        //            'user_id' => Auth::id(),
        //            'title' => request()->get('title'),
        //            'content' => request()->get('content')
        //        ]);

        return redirect()->action('Frontend\PostsController@index');
    }

    public function edit($id)
    {
        $post = Post::find($id);

        // if (Auth::id() !== $post->user_id) {
        //     return redirect()
        //         ->action('Frontend\PostsController@index')
        //         ->withErrors('沒有權限');
        // }

        return view('frontend.posts.edit', compact('post'));
    }

    public function update($id)
    {
        $this->validate(request(), [
            'title' => 'required',
            'content' => 'required'
        ]);

        $post = Post::find($id);
        // $post->title = request()->get('title');
        // $post->content = request()->get('content');
        // $post->save();

        $post->update([
            'title' => request()->get('title'),
            'content' => request()->get('content')
        ]);

        return redirect()->action('Frontend\PostsController@index');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        // Post::destroy($id);

        return redirect()->action('Frontend\PostsController@index');
    }
}