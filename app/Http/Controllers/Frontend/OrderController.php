<?php

namespace App\Http\Controllers\Frontend;

use App\Events\OrderCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Model\Entities\User\User;
use App\Model\Repositories\Order\OrderRepository;
use App\Services\Order\OrderService;
use DB;
use Exception;

class OrderController extends Controller
{

    private $orderRepository;
    private $orderService;

    /**
     * OrderController constructor.
     *
     * @param OrderRepository $orderRepository
     * @param OrderService    $orderService
     */
    public function __construct(OrderRepository $orderRepository, OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
    }

    /**
     * 儲存訂單。
     *
     * @param OrderRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OrderRequest $request)
    {
        try {
            DB::beginTransaction();

            $order = $this->orderService->createOrder($request->all());

            DB::commit();

            // 通知。
            /** @var User $user */
            $user = auth()->user();
            event(new OrderCreated($user));

            return redirect()
                ->action('Frontend\OrderController@show', $order->sn)
                ->with('success', 'Successfully created the order.');
        } catch (Exception $exception) {
            DB::rollBack();

            return redirect()
                ->back()
                ->with('error', $exception->getMessage());
        }
    }

    /**
     * 檢視訂單。
     *
     * @param string $sn
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($sn)
    {
        try {
            $order = $this->orderRepository->findBySn($sn);

            return view('frontend.orders.show', compact('order'));
        } catch (Exception $exception) {
            return redirect()
                ->back()
                ->with('error', $exception->getMessage());
        }
    }
}
