<?php

namespace App\Listeners;

use App\Events\RegisterCompleted;
use App\Mail\Welcome;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Mail;

class SendRegistrantEmail implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterCompleted $event
     *
     * @return void
     */
    public function handle(RegisterCompleted $event)
    {
        Log::info($event->user->name);

        // Mail::to($event->user)
        //     ->send(new Welcome($event->user));
    }
}
