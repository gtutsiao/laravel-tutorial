<?php

use App\Model\Entities\Post\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = factory(Post::class, 10)->create();

        foreach ($posts as $post) {
            $tagIds = array_rand([1, 2, 3, 4, 5], 2);

            $post->tags()
                 ->attach($tagIds);
        }
    }
}