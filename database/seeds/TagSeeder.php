<?php

use App\Model\Entities\Post\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create(['name' => 'Tag A']);
        Tag::create(['name' => 'Tag B']);
        Tag::create(['name' => 'Tag C']);
        Tag::create(['name' => 'Tag D']);
        Tag::create(['name' => 'Tag E']);
    }
}
